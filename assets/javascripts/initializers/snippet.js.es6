import { withPluginApi } from 'discourse/lib/plugin-api';
import ComposerController from 'discourse/controllers/composer';


function initializeSnippet(api) {

  api.addToolbarPopupMenuOptionsCallback(() => {
    return {
      action: 'insertSnippet',
      icon: 'sticky-note',
      label: 'snippet.title'
    };
  });

  ComposerController.reopen({
    actions: {
      insertSnippet() {
        this.get('toolbarEvent').applySurround(
          '\n[snippet]\n',
          '\n[/snippet]\n',
          'snippet_text',
          { multiline: true }
        );
      }
    }
  });
}

export default {
  name: 'apply-snippets',
  initialize(container) {
    const siteSettings = container.lookup('site-settings:main');
    if (siteSettings.snippet_enabled) {
      withPluginApi('0.5', initializeSnippet);
    }
  }
};
