export function setup(helper) {
  helper.whiteList(['div.discourse-snippet']);

  helper.registerOptions((opts, siteSettings) => {
    opts.features['snippet'] = !!siteSettings.snippet_enabled;
  });

  helper.registerPlugin(md => {
    md.block.bbcode.ruler.push('snippet', {
      tag: 'snippet',
      wrap: function(token, tagInfo) {
        token.attrs = [
          ['class', 'discourse-snippet'],
          ['data-name', tagInfo.attrs['name'] || 'Text Blaze Snippet']
        ];

        if (tagInfo.attrs['shortcut']) {
          token.attrs.push(['data-shortcut', tagInfo.attrs['shortcut']]);
        }

        if (tagInfo.attrs['quickentry']) {
          token.attrs.push(['data-quickentry', tagInfo.attrs['quickentry']]);
        }

        return true;
      }
    });
  });
}
